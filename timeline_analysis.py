
# coding: utf-8

# In[93]:


import json
import pandas as pd
import numpy as np
from pandas.io.json import json_normalize


# In[94]:


with open("timeline.ctf.json",'r') as load_f:
    load_dict = json.load(load_f)
    load = json_normalize(load_dict['traceEvents'])
    df = pd.DataFrame.from_dict(load)
    df = df[3:-1]
    df = df.reset_index(drop=True)
    
    #drop some attributes we don't need
    df = df.drop(['name', 'args.input0', 'args.input1', 'args.input2', 'tid', 'pid', 'id', 'ph'], axis=1)
    
total_time = df.iloc[:, np.where(result.columns=='dur')[0]].sum()
print('Total time consumption is %.3f ms.' % (total_time/1000))
print(df)


# In[102]:


#select one layer
layer = 'conv_pw_1_bn_2'
result = df[df['args.name'].str.match(layer ,na=False)]
print(result)


# In[104]:


#sorting with some attribute of this layer
att = 'dur'
result_sort = result.sort_values([att], ascending=False)
print(result_sort.head(8))


# In[101]:


#time consumption of this layer
time = result.iloc[:, np.where(result.columns=='dur')[0]].sum()
print('Time consumption is %.3f ms.' % (time/1000))


# In[98]:


#Investigate each layer of mobilenetv2
layer1 = 'conv1_2'
layer2 = 'conv1_bn_2'
layer3 = 'conv1_relu_2'

result = df[df['args.name'].str.match(layer1 ,na=False)]
time = result.iloc[:, np.where(result.columns=='dur')[0]].sum()
print('%s : %.3f ms.(%.3f%%)' % (layer1, time/1000, (100*time/total_time)))
    
result = df[df['args.name'].str.match(layer2 ,na=False)]
time = result.iloc[:, np.where(result.columns=='dur')[0]].sum()
print('%s : %.3f ms.(%.3f%%)' % (layer2, time/1000, (100*time/total_time)))
    
result = df[df['args.name'].str.match(layer3 ,na=False)]
time = result.iloc[:, np.where(result.columns=='dur')[0]].sum()
print('%s : %.3f ms.(%.3f%%)' % (layer3, time/1000, (100*time/total_time)))

for i in range(13):
    layer1_dw = 'conv_dw_' + str(1+i) + '_2'
    layer2_dw = 'conv_dw_' + str(1+i) + '_bn_2'
    layer3_dw = 'conv_dw_' + str(1+i) + '_relu_2'
    layer1_pw = 'conv_pw_' + str(1+i) + '_2'
    layer2_pw = 'conv_pw_' + str(1+i) + '_bn_2'
    layer3_pw = 'conv_pw_' + str(1+i) + '_relu_2'
    
    result = df[df['args.name'].str.match(layer1_dw ,na=False)]
    time = result.iloc[:, np.where(result.columns=='dur')[0]].sum()
    print('%s : %.3f ms.(%.3f%%)' % (layer1_dw, time/1000, (100*time/total_time)))
    
    result = df[df['args.name'].str.match(layer2_dw ,na=False)]
    time = result.iloc[:, np.where(result.columns=='dur')[0]].sum()
    print('%s : %.3f ms.(%.3f%%)' % (layer2_dw, time/1000, (100*time/total_time)))
    
    result = df[df['args.name'].str.match(layer3_dw ,na=False)]
    time = result.iloc[:, np.where(result.columns=='dur')[0]].sum()
    print('%s : %.3f ms.(%.3f%%)' % (layer3_dw, time/1000, (100*time/total_time)))
    
    result = df[df['args.name'].str.match(layer1_pw ,na=False)]
    time = result.iloc[:, np.where(result.columns=='dur')[0]].sum()
    print('%s : %.3f ms.(%.3f%%)' % (layer1_pw, time/1000, (100*time/total_time)))
    
    result = df[df['args.name'].str.match(layer2_pw ,na=False)]
    time = result.iloc[:, np.where(result.columns=='dur')[0]].sum()
    print('%s : %.3f ms.(%.3f%%)' % (layer2_pw, time/1000, (100*time/total_time)))
    
    result = df[df['args.name'].str.match(layer3_pw ,na=False)]
    time = result.iloc[:, np.where(result.columns=='dur')[0]].sum()
    print('%s : %.3f ms.(%.3f%%)' % (layer3_pw, time/1000, (100*time/total_time)))
    

