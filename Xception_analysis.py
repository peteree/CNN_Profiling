
# coding: utf-8

# In[1]:


import json
import pandas as pd
import numpy as np
from pandas.io.json import json_normalize


# In[3]:


with open("timeline_Xception.ctf.json",'r') as load_f:
    load_dict = json.load(load_f)
    load = json_normalize(load_dict['traceEvents'])
    df = pd.DataFrame.from_dict(load)
    df = df[3:-1]
    df = df.reset_index(drop=True)
    
    #drop some attributes we don't need
    df = df.drop(['name', 'args.input0', 'args.input1', 'args.input2', 'tid', 'pid', 'id', 'ph', 'cat', 'ts'], axis=1)
    
total_time = df.iloc[:, np.where(df.columns=='dur')[0]].sum()
print('Total time consumption is %.3f ms.' % (total_time/1000))
print(df)


# In[5]:


#sorting with some attribute of this model
att = 'dur'
df_sort = df.sort_values([att], ascending=False)
print(df_sort)


# In[6]:


#select one layer
layer = 'block4_sepconv2/'
result = df[df['args.name'].str.match(layer ,na=False)]
print(df[df['args.name'].str.match(layer ,na=False)])


# In[7]:


#sorting with some attribute of this layer
att = 'dur'
result_sort = result.sort_values([att], ascending=False)
print(result_sort)

